use std::env;
use std::fs;
use std::io::Error;

pub fn read_file(path: &str) -> Result<String, Error> {
    let cwd_buf = env::current_dir().expect("Not found current dir.");
    let cwd = cwd_buf.to_str().expect("Expected string reference.");

    let content = fs::read_to_string(cwd.to_owned() + path);

    return content;
}
