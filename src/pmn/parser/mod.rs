use crate::pmn::ast::expr::{BinaryOperator, Expr, UnaryOperator};
use crate::pmn::error::Error;
use crate::pmn::scanner::token::{Token, TokenType};

pub struct Parser {
    tokens: Vec<Token>,
    current: u32,
}

impl Parser {
    pub fn new(tokens: Vec<Token>) -> Parser {
        Parser {
            tokens: tokens,
            current: 0,
        }
    }

    pub fn parse(&mut self) -> Expr {
        let expr = self.expression();

        match expr {
            Ok(expression) => return expression,
            Err(err) => {
                panic!("[parser-panic] {}", err)
            }
        }
    }

    pub fn expression(&mut self) -> Result<Expr, Error> {
        return self.term();
    }

    pub fn term(&mut self) -> Result<Expr, Error> {
        let expr_result = self.factor();
        let mut expr: Expr;

        if let Ok(rexpr) = expr_result {
            expr = rexpr;
        } else {
            return expr_result;
        }

        while self.match_with(vec![TokenType::PLUS, TokenType::MINUS]) {
            let operator_result = self.previous();
            let mut bin_operator = BinaryOperator::NIL;

            if let Ok(operator) = operator_result {
                match operator._type {
                    TokenType::PLUS => bin_operator = BinaryOperator::ADD,
                    TokenType::MINUS => bin_operator = BinaryOperator::SUB,
                    _ => {}
                }
            } else if let Err(err) = operator_result {
                return Err(err);
            }

            let right_result = self.factor();

            if let Ok(right) = right_result {
                match bin_operator {
                    BinaryOperator::NIL => {
                        return Err(Error {
                            msg: String::from("term binary operator was not define"),
                        });
                    }
                    _ => {
                        expr = Expr::Binary(Box::new(expr), bin_operator, Box::new(right));
                    }
                }
            } else {
                return right_result;
            }
        }

        Ok(expr)
    }

    pub fn factor(&mut self) -> Result<Expr, Error> {
        let expr_result = self.unary();
        let mut expr: Expr;

        if let Ok(rexpr) = expr_result {
            expr = rexpr;
        } else {
            return expr_result;
        }

        while self.match_with(vec![TokenType::STAR, TokenType::SLASH]) {
            let operator_result = self.previous();
            let mut bin_operator = BinaryOperator::NIL;

            if let Ok(operator) = operator_result {
                match operator._type {
                    TokenType::STAR => bin_operator = BinaryOperator::MUL,
                    TokenType::SLASH => bin_operator = BinaryOperator::DIV,
                    _ => {}
                }
            } else if let Err(err) = operator_result {
                return Err(err);
            }

            let right_result = self.unary();

            if let Ok(right) = right_result {
                match bin_operator {
                    BinaryOperator::NIL => {
                        return Err(Error {
                            msg: String::from("factor binary operator was not define"),
                        });
                    }
                    _ => {
                        expr = Expr::Binary(Box::new(expr), bin_operator, Box::new(right));
                    }
                }
            } else {
                return right_result;
            }
        }

        Ok(expr)
    }

    pub fn unary(&mut self) -> Result<Expr, Error> {
        if self.match_with(vec![TokenType::MINUS]) {
            let expr = self.unary();
            if let Ok(uexpr) = expr {
                return Ok(Expr::Unary(UnaryOperator::MINUS, Box::new(uexpr)));
            }
            return expr;
        }

        self.literal()
    }

    pub fn literal(&mut self) -> Result<Expr, Error> {
        if self.match_with(vec![TokenType::NUMBER]) {
            if let Ok(token) = self.previous() {
                return Ok(Expr::Literal(token.literal));
            }
        }

        if self.match_with(vec![TokenType::LPAREN]) {
            let expr_result = self.expression();
            if let Err(err) = self.consume(TokenType::RPAREN, "") {
                return Err(err);
            }

            match expr_result {
                Ok(expr) => return Ok(Expr::Grouping(Box::new(expr))),
                Err(err) => return Err(err),
            }
        }

        Err(Error {
            msg: String::from("Literal not found."),
        })
    }

    // utils

    pub fn is_at_end(&self) -> bool {
        if let Ok(token) = self.peek() {
            return token._type == TokenType::EOF;
        }
        return false;
    }

    pub fn advance(&mut self) {
        if !self.is_at_end() {
            self.current += 1;
        }
    }

    pub fn previous(&self) -> Result<Token, Error> {
        let current_usize_result = usize::try_from(self.current - 1);
        match current_usize_result {
            Ok(crt) => {
                let token_result = self.tokens.get(crt);
                match token_result {
                    Some(token) => {
                        return Ok(token.to_owned());
                    }
                    None => {
                        return Err(Error {
                            msg: String::from(format!(
                                "peek previous failed cause no token was found in {}",
                                crt
                            )),
                        });
                    }
                }
            }
            Err(_) => {
                return Err(Error {
                    msg: String::from(
                        "peek previous failed cause of current convertion to usize failed.",
                    ),
                });
            }
        }
    }

    pub fn peek(&self) -> Result<Token, Error> {
        let current_usize_result = usize::try_from(self.current);
        match current_usize_result {
            Ok(crt) => {
                let token_result = self.tokens.get(crt);
                match token_result {
                    Some(token) => {
                        return Ok(token.to_owned());
                    }
                    None => {
                        return Err(Error {
                            msg: String::from(format!(
                                "peek failed cause no token was found in {}",
                                crt
                            )),
                        });
                    }
                }
            }
            Err(_) => {
                return Err(Error {
                    msg: String::from("peek failed cause of current convertion to usize failed."),
                });
            }
        }
    }

    pub fn check(&self, _type: TokenType) -> bool {
        if self.is_at_end() {
            return false;
        }
        if let Ok(token) = self.peek() {
            return token._type == _type;
        }

        return false;
    }

    pub fn match_with(&mut self, types: Vec<TokenType>) -> bool {
        for _type in types {
            if self.check(_type) == true {
                self.advance();
                return true;
            }
        }

        return false;
    }

    pub fn consume(&mut self, _type: TokenType, message: &str) -> Result<(), Error> {
        if self.check(_type) {
            self.advance();
            return Ok(());
        }

        Err(Error {
            msg: String::from(message),
        })
    }
}
