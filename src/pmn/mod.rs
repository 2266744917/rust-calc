pub mod scanner;
pub mod parser;
pub mod ast;
pub mod error;

use crate::utils::file::read_file;
use crate::pmn::scanner::Scanner;
use crate::pmn::scanner::token::{Token};
use self::ast::expr::Expr;
use self::error::Error;
use self::parser::Parser;
pub struct Pmn {
    source_path: String
}

impl Pmn {
    pub fn new(source_path: String) -> Pmn {
        Pmn { source_path }
    }

    fn run(&mut self, source: String) {
        let tokens = self.scanner(source);
        let ast = self.parser(tokens);
        println!("AST: {:?}", ast);

    }

    fn scanner(&self, source: String) -> Vec<Token> {
        let mut scanner = Scanner::new(source);
        scanner.scan().clone()
    }

    #[allow(dead_code)]
    fn parser( &self, tokens: Vec<Token>) -> Expr {
        let mut parser = Parser::new(tokens);
        parser.parse()
    }

    pub fn start(&mut self) {
        let content = self.load_source();

        if let Err(err) = content {
            println!("[Pmn.start] Error: {}", err);
            return;
        }

        if let Ok(source) = content {
            self.run(source);
        }
    }

    fn load_source(&mut self) -> Result<String, Error> {
        let content = read_file(self.source_path.as_str());
    
        if let Err(err) = content {
            println!("Error: {}", err);
            return Err( Error{ msg: String::from("[pmn]file failed to load.") } );
        }

        return Ok(content.unwrap());
    }
}