use crate::pmn::scanner::token::LiteralValue;

#[derive(Debug)]
pub enum Expr {
    Binary(Box<Expr>, BinaryOperator, Box<Expr>),
    Unary(UnaryOperator, Box<Expr>),
    Grouping(Box<Expr>),
    Literal(LiteralValue)
}

#[derive(Debug)]
pub enum BinaryOperator {
    MUL, DIV, ADD, SUB, NIL
}

#[derive(Debug)]
pub enum UnaryOperator {
    MINUS
}