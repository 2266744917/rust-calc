use std::fmt::Display;

#[derive(Clone, Debug)]
pub enum LiteralValue {
    NUMBER(f64),
    NIL
}

#[derive(Clone)]
pub struct Token {
    pub _type: TokenType,
    pub lexeme: String,
    pub literal: LiteralValue,
    pub line: u32
}

impl Token {
    pub fn new(_type: TokenType, line: u32, lexeme: String, literal: LiteralValue) -> Token {
        Token { _type: _type, lexeme: lexeme, literal: literal, line: line }
    }
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<[{}] Token '{}'>", convert_tokentype_to_string(&self._type), self.lexeme)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum TokenType {
    LPAREN, RPAREN,
    PLUS, MINUS, STAR, SLASH,
    EOF, NUMBER
}

pub fn convert_tokentype_to_string(_type: &TokenType) -> String {
    match _type {
        TokenType::LPAREN => { String::from("(") }
        TokenType::RPAREN => { String::from(")") }
        TokenType::PLUS => { String::from("+") }
        TokenType::MINUS => { String::from("-") }
        TokenType::STAR => { String::from("*") }
        TokenType::SLASH => { String::from("/") }
        TokenType::EOF => { String::from("EndOfLine") }
        TokenType::NUMBER => { String::from("int") }
    }
    
}