pub mod token;
use self::token::{convert_tokentype_to_string, LiteralValue, Token, TokenType};
use crate::pmn::Error;
use regex::Regex;
use std::convert::TryFrom;

pub struct Scanner {
    source: String,
    current: u32,
    start: u32,
    line: u32,
    tokens: Vec<Token>,
}

impl Scanner {
    pub fn new(source: String) -> Scanner {
        Scanner {
            source: source,
            current: 0,
            start: 0,
            line: 1,
            tokens: vec![],
        }
    }

    pub fn scan(&mut self) -> &Vec<Token> {
        while self.is_at_end() == false {
            let scan_result = self.scan_token();

            if let Err(err) = scan_result {
                panic!("[scanner-panic] {}", err);
            }

            self.start = self.current;
        }

        &self.tokens
    }

    fn scan_token(&mut self) -> Result<(), Error> {
        let cre = self.advance();

        #[allow(unused_must_use)]
        if let Ok(c) = cre {
            match c {
                '(' => {
                    self.add_token(TokenType::LPAREN, LiteralValue::NIL);
                    return Ok(());
                }
                ')' => {
                    self.add_token(TokenType::RPAREN, LiteralValue::NIL);
                    return Ok(());
                }
                '+' => {
                    self.add_token(TokenType::PLUS, LiteralValue::NIL);
                    return Ok(());
                }
                '-' => {
                    self.add_token(TokenType::MINUS, LiteralValue::NIL);
                    return Ok(());
                }
                '*' => {
                    self.add_token(TokenType::STAR, LiteralValue::NIL);
                    return Ok(());
                }
                '/' => {
                    self.add_token(TokenType::SLASH, LiteralValue::NIL);
                    return Ok(());
                }
                ' ' => return Ok(()),
                '\n' => {
                    self.line += 1;
                    return Ok(());
                }
                _ => {
                    let is_number_result = self.is_digit(c);

                    if let Ok(is_number) = is_number_result {
                        if is_number {
                            let number_result = self.number();
                            if let Err(err) = number_result {
                                return Err(err);
                            }
                            return Ok(());
                        }
                    } else if let Err(err) = is_number_result {
                        return Err(err);
                    }

                    self.warn(format!("unsupported character: {}", c));
                }
            }
        }

        if let Err(err) = cre {
            return Err(err);
        }

        return Err(Error {
            msg: String::from("token scanner cases failed."),
        });
    }

    // factory

    fn number(&mut self) -> Result<(), Error> {
        // check outs every number until a non-number appears;
        let mut char_result = self.peek();

        'root: loop {
            match char_result {
                Ok(chr) => match self.is_digit(chr) {
                    Ok(is_digit) => {
                        if is_digit == true && self.is_at_end() == false {
                            self.current += 1;
                            char_result = self.peek();
                        } else {
                            break 'root;
                        }
                    }
                    Err(err) => return Err(err),
                },
                Err(_) => {
                    break 'root;
                }
            }
        }

        // checks out every number after the dot
        match self.peek() {
            Ok(chrt) => {
                if chrt == '.' {
                    self.current += 1;
                    let mut char_result = self.peek();
                    'dotroot: loop {
                        if self.is_at_end() {
                            break 'dotroot;
                        }

                        match char_result {
                            Ok(chr) => match self.is_digit(chr) {
                                Ok(is_digit) => {
                                    if is_digit == true && self.is_at_end() == false {
                                        self.current += 1;
                                        char_result = self.peek();
                                        continue 'dotroot;
                                    } else {
                                        break 'dotroot;
                                    }
                                }
                                Err(err) => return Err(err),
                            },
                            Err(_) => {
                                break 'dotroot;
                            }
                        }
                    }
                }
            }
            Err(_) => {}
        }

        let sindex = usize::try_from(self.start).ok();
        let cindex = usize::try_from(self.current).ok();
        if let (Some(indx), Some(cindx)) = (sindex, cindex) {
            let lexeme = String::from(&self.source[indx..cindx]);

            let to_number_result = lexeme.parse::<f64>();

            if let Ok(num) = to_number_result {
                self.add_token(TokenType::NUMBER, LiteralValue::NUMBER(num));
                return Ok(());
            } else {
                return Err(Error {
                    msg: String::from(format!("number '{}' fail to be parse.", lexeme)),
                });
            }
        }

        return Err(Error {
            msg: String::from("number parse cases fail."),
        });
    }

    // validatos

    fn is_digit(&self, c: char) -> Result<bool, Error> {
        let re = Regex::new("[0-9]");
        if let Ok(regx) = re {
            return Ok(regx.is_match(&String::from(c)));
        } else if let Err(_err) = re {
            return Err(Error {
                msg: String::from("Unexpeced regex error"),
            });
        }

        Err(Error {
            msg: String::from("Scanner.is_digit; (var) [regex_result] cases unexpected failure."),
        })
    }

    // utils

    fn warn(&self, message: String) {
        println!("[scanner-warn] {}", message);
    }

    fn peek(&self) -> Result<char, Error> {
        let index = usize::try_from(self.current);
        if let Ok(result) = index {
            // println!("{} : {}", result, self.source.len());
            let char = self.source.chars().nth(result);
            if let Some(c) = char {
                return Ok(c);
            }
        }

        return Err(Error {
            msg: String::from("could not peek in source."),
        });
    }

    fn add_token(&mut self, _type: TokenType, literal: LiteralValue) {
        let sindex = usize::try_from(self.start).ok();
        let cindex = usize::try_from(self.current).ok();
        if let (Some(indx), Some(cindx)) = (sindex, cindex) {
            let lexeme = &self.source[indx..cindx];
            self.tokens
                .push(Token::new(_type, self.line, String::from(lexeme), literal));
            return;
        }
        self.warn(format!(
            "token type {} was not save",
            convert_tokentype_to_string(&_type)
        ));
    }

    fn is_at_end(&self) -> bool {
        let length_try = u32::try_from(self.source.len());
        if let Ok(length) = length_try {
            return self.current >= length;
        }
        return false;
    }

    fn advance(&mut self) -> Result<char, Error> {
        let index = usize::try_from(self.current);
        if let Ok(result) = index {
            let char = self.source.chars().nth(result);
            if let Some(c) = char {
                self.current += 1;
                return Ok(c);
            }
        }

        return Err(Error {
            msg: String::from("could not advance in source."),
        });
    }
}
