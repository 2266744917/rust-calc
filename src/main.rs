extern crate rust_calc;
use rust_calc::pmn::Pmn;

fn main() {
    let mut pmn = Pmn::new(String::from("/test/main.pmn"));
    pmn.start();
}
